/*
    ./server.js
    Main file for server
*/

// env config
require('dotenv').config()

// Imports
const express = require('express')
const app = express()
var path = require('path');
var bodyParser = require('body-parser');

// constants
const PORT = process.env.PORT || 5001

// middlewares
// use json
app.use(express.json())
// view engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
// statis dir
app.use(express.static(path.join(__dirname, 'public')));
//
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

// api routes
const welcomeRouter = require('./routes/welcome')
app.use('/', welcomeRouter)
const apiRouter = require('./routes/api')
app.use('/api', apiRouter)
const contractsRouter = require('./routes/contracts')
app.use('/api/contracts', contractsRouter)
// html routes
const settingsRouter = require('./routes/settings')
app.use('/settings', settingsRouter)

// start express application
app.listen(PORT, () => console.log('server started'))

// export application
module.exports = app