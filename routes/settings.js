/*
    ./routes/settings.js
    Router for Settings views
*/

// Imports
const express = require('express')
const router = express.Router()
const fs = require('fs');
const mongoose = require('mongoose');

router.get('/', (req, res) => {
    res.render('settings');
})

router.post('/contracts', (req, res) => {
    console.log(req.body)
    const newContract = {
        collectiveContractNumber: req.body.ccn,
        employerCode: req.body.empCode,
        year: parseInt(req.body.year),
        month: parseInt(req.body.month),
        codpar: req.body.codpar,
        codparLabel: req.body.codparLabel,
        amounts: {
            due: parseInt(req.body.due),
            recall: parseInt(req.body.recall),
            overPayment: parseInt(req.body.overPayment)
        }
    }
    console.log(newContract)
    let rawdata = fs.readFileSync('./data/contracts.json');
    let contracts = JSON.parse(rawdata);
    contracts.push(newContract);
    console.log("Contract added to contracts list")
    fs.writeFileSync('./data/contracts.json', JSON.stringify(contracts));
    console.log("data file updated !")
    res.redirect('/settings/contracts/')
})

router.get('/contracts', (req, res) => {
    let rawdata = fs.readFileSync('./data/contracts.json');
    let contracts = JSON.parse(rawdata);
    res.render('contracts', { "contracts": contracts });
})

router.get('/contracts/:ccn/:empCode/:year/:month/:codpar', (req, res) => {

    console.log("Get contract details with params :")
    console.log(JSON.stringify(req.params))

    let rawdata = fs.readFileSync('./data/contracts.json');
    let contracts = JSON.parse(rawdata);
    var found = false;
    
    for (var i = 0; i < contracts.length; i++) {

        if(found) break;
        
        if (!found && contracts[i].collectiveContractNumber === req.params.ccn) {
            console.log("Contract found with the required ccn")
            if (contracts[i].employerCode === req.params.empCode) {
                console.log("Contract found with the required empCode")
                if (contracts[i].year === parseInt(req.params.year)) {
                    console.log("Contract found with the required year")
                    if (contracts[i].month === parseInt(req.params.month)) {
                        console.log("Contract found with the required month")
                        if (contracts[i].codpar === req.params.codpar) {
                            console.log("Contract found with the required attributes !")
                            found = true;
                            res.render('contract', { "contract": contracts[i] });
                        }
                    }
                }
            }
        }
    }

    if(!found){
        res.status(404).json({
            "message": "no contract with required info"
        });
    }
})

router.post('/contracts/:ccn/:empCode/:year/:month/:codpar', (req, res) => {
    let rawdata = fs.readFileSync('./data/contracts.json');
    let contracts = JSON.parse(rawdata);
    var found = false;
    
    for (var i = 0; i < contracts.length; i++) {

        if(found) break;
        
        if (!found && contracts[i].collectiveContractNumber === req.params.ccn) {
            if (contracts[i].employerCode === req.params.empCode) {
                if (contracts[i].year === parseInt(req.params.year)) {
                    if (contracts[i].month === parseInt(req.params.month)) {
                        if (contracts[i].codpar === req.params.codpar) {
                            found = true;
                            contracts[i].collectiveContractNumber = req.body.ccn;
                            contracts[i].employerCode = req.body.empCode;
                            contracts[i].year = parseInt(req.body.year);
                            contracts[i].month = parseInt(req.body.month);
                            contracts[i].codpar = req.body.codpar;
                            contracts[i].codparLabel = req.body.codparLabel;
                            contracts[i].amounts.due = parseInt(req.body.due);
                            contracts[i].amounts.recall = parseInt(req.body.recall);
                            contracts[i].amounts.overPayment = parseInt(req.body.overPayment);
                            fs.writeFileSync('./data/contracts.json', JSON.stringify(contracts));
                            res.redirect('/settings/contracts/')
                        }
                    }
                }
            }
        }
    }

    if(!found){
        res.status(404).json({
            "message": "no contract with required info"
        });
    }
})

router.get('/contracts/:ccn/:empCode/:year/:month/:codpar/delete', (req, res) => {

    let rawdata = fs.readFileSync('./data/contracts.json');
    let contracts = JSON.parse(rawdata);
    var found = false;
    
    for (var i = 0; i < contracts.length; i++) {

        if(found) break;
        
        if (!found && contracts[i].collectiveContractNumber === req.params.ccn) {
            if (contracts[i].employerCode === req.params.empCode) {
                if (contracts[i].year === parseInt(req.params.year)) {
                    if (contracts[i].month === parseInt(req.params.month)) {
                        if (contracts[i].codpar === req.params.codpar) {
                            found = true;
                            contracts.splice(i, 1);
                            fs.writeFileSync('./data/contracts.json', JSON.stringify(contracts));
                            res.redirect('/settings/contracts')
                        }
                    }
                }
            }
        }
    }

    if(!found){
        res.status(404).json({
            "message": "no contract with required info"
        });
    }
})

module.exports = router