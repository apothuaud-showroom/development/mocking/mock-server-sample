/*
    ./routes/api.js
    Router for Welcome endpoints
*/

// Imports
const express = require('express')
const router = express.Router()

// Display API information
router.get('/', (req, res) => {
    res.json({
        "name": "[EC] Mocked API",
        "version": "0.0.1",
        "description": "API Mockée de l'éspace collectivités. Fournit des endpoints avec des ressources contrôlées.",
        "links": [
            {
                "name": "contracts",
                "href": req.protocol + '://' + req.get('host') + "/api/contracts"
            }
        ],
        "author": "Adrian Pothuaud"
    })
})

module.exports = router