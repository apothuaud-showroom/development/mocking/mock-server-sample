/*
    ./routes/contracts.js
    Router for Contracts endpoints
*/

// Imports
const express = require('express')
const router = express.Router()
const fs = require('fs');

// Get all contracts
router.get('/', async (req, res) => {
    let rawdata = fs.readFileSync('./data/contracts.json');
    let contracts = JSON.parse(rawdata);
    res.status(200).json(contracts);
})

// export Router
module.exports = router
