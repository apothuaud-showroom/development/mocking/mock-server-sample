/*
    ./routes/welcome.js
    Router for Welcome endpoints
*/

// Imports
const express = require('express')
const router = express.Router()

// Display API information
router.get('/', (req, res) => {
    res.json({
        "name": "[EC] Mock Server",
        "version": "0.0.1",
        "description": "API Mock Server de l'éspace collectivités. Fournit des endpoints avec des ressources contrôlées et des vues HTML pour manipuler les ressources en question.",
        "links": [
            {
                "name": "API",
                "href": req.protocol + '://' + req.get('host') + "/api"
            },
            {
                "name": "Settings",
                "href": req.protocol + '://' + req.get('host') + "/settings"
            }
        ],
        "author": "Adrian Pothuaud"
    })
})

module.exports = router