/// <reference types="Cypress" />

describe('Settings Bikes List', () => {

    beforeEach(() => {
        cy.visit('/settings/bikes')
    })

    it('shows navigation links', () => {
        cy.get('#settingsLnk').should('have.attr', 'href', '/settings')
        cy.get('#bikesLnk').should('have.attr', 'href', '/settings/bikes').and('have.class', 'active')
    })

    it('greets with heading', () => {
        cy.get('#heading h1').should('have.text', 'Settings')
        cy.get('#heading p').should('have.text', 'Settings for bikes data')
    })

    it('shows bikes list', () => {

    })

    it('show bike form', () => {
        
    })

})